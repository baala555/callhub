package pages;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import utils.Constants;

public class AddToCart {
	
	private static final Logger logger = Logger.getLogger(AddToCart.class.getName());
	private String CLASSNAME = this.getClass().getName();
	
	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _2Npkh4 _2MWPVK']")
	WebElement addToCart;
	
	@FindBy(how = How.XPATH, using = "(//div[@class='gdUKd9']//span)[2]")
	WebElement removeItem;
	
	

	public AddToCart(WebDriver driver) {           
        Constants.driver = driver; 
        PageFactory.initElements(driver, this);
        
	}
	
	public boolean addToCart()
	{
		String METHODNAME = "addToCart";
		logger.info("Entering method "+METHODNAME+"class +"+CLASSNAME);
		Constants.driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS) ;

		ArrayList<String> tabList = new ArrayList<String> (Constants.driver.getWindowHandles());
		logger.info(tabList +"--" +tabList.size());
		logger.info("Title of Tab"+Constants.driver.getTitle());
		Constants.driver.switchTo().window(tabList.get(1));
		addToCart.click();
		logger.info("Exiting method "+METHODNAME+"class +"+CLASSNAME);
		return true;
	}
	
	public boolean takeScreenShoot() throws InterruptedException
	{
		String METHODNAME = "takeScreenShoot";
		logger.info("Entering method "+METHODNAME+"class +"+CLASSNAME);
		Thread.sleep(1000);
		TakesScreenshot scrShot =((TakesScreenshot) Constants.driver);
		File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
		String currentDir = System.getProperty("user.dir");
		String fileWithPath = currentDir+"/drivers/flipkart.png";
		File DestFile=new File(fileWithPath);
		try {
			FileUtils.copyFile(SrcFile, DestFile);
		} catch (IOException e) {
			logger.info("error occured in class : " + this.getClass().getName()
					+ ", methodName : takeScreenShoot() with errors " + e.getMessage());
		}
		logger.info("Exiting method "+METHODNAME+"class +"+CLASSNAME);
		return true;

		
	}
	
	
	public boolean removeItem()
	{
		String METHODNAME = "removeItem";
		logger.info("Entering method "+METHODNAME+"class +"+CLASSNAME);
		removeItem.click();
		logger.info("Exiting method "+METHODNAME+"class +"+CLASSNAME);
		return true;
	}
}
