package pages;

import java.util.logging.Logger;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Constants;

public class UserLoginPage{
	
	private static final Logger logger = Logger.getLogger(UserLoginPage.class.getName());
	private String CLASSNAME = this.getClass().getName();
	
	@FindBy(how = How.XPATH, using = "//div[@class='dHGf8H']//a")
	WebElement loginButton;
	
	@FindBy(how = How.XPATH, using = "//input[@class='_2zrpKA']")
	WebElement userName;
	
	@FindBy(how = How.XPATH, using = "//input[@class='_2zrpKA _3v41xv']")
	WebElement password;
	
	@FindBy(how = How.XPATH, using = "//button[@class='_2AkmmA _1LctnI _7UHT_c']")
	WebElement login;
	
	@FindBy(how = How.XPATH, using = "//div[@class='_2aUbKa']")
	WebElement myAccount;
	
	
	WebDriverWait wait; 
	
	
	public UserLoginPage(WebDriver driver) {           
        Constants.driver = driver; 
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver,30);
}
	
	
	public boolean registeredUserLogin() {
		String METHODNAME = "registeredUserLogin";
		logger.info("Entering method " + METHODNAME + "class +" + CLASSNAME);
		try {
			userName.sendKeys(Constants.PHONE_NUMBER);
			if (!password.isDisplayed())
				login.click();
			wait.until(ExpectedConditions.visibilityOf(password));
			password.sendKeys(Constants.PASSWORD);
			login.click();
			return true;
		} catch (Exception e) {
			logger.warning(
					"error occured in class : " + CLASSNAME + ", methodName : " + METHODNAME + " , " + e.getMessage());
			return false;
		}

	}


}
