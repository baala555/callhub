package pages;

import java.util.List;
import java.util.logging.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.Constants;

public class ProductSearch {
	
	private static final Logger logger = Logger.getLogger(ProductSearch.class.getName());
	
	
	private String CLASSNAME = this.getClass().getName();


	@FindBy(how = How.XPATH, using = "//span[@class='_1QZ6fC _3Lgyp8']")
	WebElement searchBar;

	@FindBy(how = How.XPATH, using = "//li[@class='_2GG4xt']")
	WebElement dropDownList;

	@FindBy(how = How.XPATH, using = "(//div[@class='NYE9b8 col col-4-12'])[3]")
	WebElement selectCategory;

	@FindBy(how = How.XPATH, using = "//div[@class='_3wU53n']")
	WebElement selectProduct;
	
	WebDriverWait wait; 


	public ProductSearch(WebDriver driver) {
		Constants.driver = driver;
		PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver,30);

	}

	public boolean searchProduct() throws InterruptedException{
		String METHODNAME = "searchProduct";
		logger.info("Entering method "+METHODNAME+"class +"+CLASSNAME);
		Thread.sleep(1000);
		searchBar.click();
		List<WebElement> listOfElements = dropDownList.findElements(By.tagName("a"));
		for (WebElement selectList : listOfElements) {
			if (selectList.getText().contains(Constants.MOBILE_COMPANY)) {
				selectList.click();
				break;
			}
		}
		Thread.sleep(1000);
		selectCategory.click();
		Thread.sleep(1000);
		List<WebElement> listOfItems = Constants.driver.findElements(By.xpath("//div[@class='_3wU53n']"));
		logger.info("lists of itemas are +"+listOfItems);
		for (WebElement selectList : listOfItems) {
			logger.info(selectList.getText());
			if (selectList.getText().contains(Constants.MOBILE_NAME)) {
				selectList.click();
				break;
			}
		}
		logger.info("Exiting method "+METHODNAME+"class +"+CLASSNAME);
		return true;
	}

}
