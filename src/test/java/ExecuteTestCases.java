import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import junit.framework.Assert;
import pages.AddToCart;
import pages.ProductSearch;
import pages.UserLoginPage;
import utils.Constants;

public class ExecuteTestCases{
	
public static  WebDriver driver = null;
	

@BeforeSuite
 public void initialize() throws IOException{
 
	String currentDir = System.getProperty("user.dir");
	System.setProperty("webdriver.chrome.driver", currentDir + "/drivers/chromedriver_mac");
	ChromeOptions options = new ChromeOptions();
	options.addArguments("--incognito");
	DesiredCapabilities capabilities = DesiredCapabilities.chrome();
	capabilities.setCapability(ChromeOptions.CAPABILITY, options);	
	driver = (WebDriver) new ChromeDriver(capabilities);
    driver.get(Constants.URL);
 }
	
	
	@Test(priority = 0)
	public void login() throws InterruptedException, IOException
	{
		UserLoginPage loginObj = new UserLoginPage(driver);
		boolean testLogin = loginObj.registeredUserLogin();
		Assert.assertEquals(true, testLogin);
	}	
	
	@Test(priority = 1)
	public void searchForCategory() throws InterruptedException
	{
		ProductSearch searchObject = new ProductSearch(driver);
		boolean testSearch = searchObject.searchProduct();
		Assert.assertEquals(true, testSearch);
		}
		
	
	@Test(priority = 2)
	public void addToCart()
	{
		AddToCart addToCartObject = new AddToCart(driver);
		boolean testAddToCart = addToCartObject.addToCart();
		Assert.assertEquals(true, testAddToCart);
	}
	
	@Test(priority = 3)
	public void takeScreenShoot() throws InterruptedException
	{
		AddToCart screenShotObject = new AddToCart(driver);
		boolean testScreenShot = screenShotObject.takeScreenShoot();
		Assert.assertEquals(true, testScreenShot);
	}
	
	@Test(priority = 4)
	public void removeItem()
	{

		AddToCart removeItemObject = new AddToCart(driver);
		boolean testRemoveItem = removeItemObject.removeItem();
		Assert.assertEquals(true, testRemoveItem);

	}
	
	@AfterSuite
	public void close() {
		driver.quit();
	}
	
	

}
